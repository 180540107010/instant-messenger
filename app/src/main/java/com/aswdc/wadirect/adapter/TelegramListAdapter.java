package com.aswdc.wadirect.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc.wadirect.R;
import com.aswdc.wadirect.model.TelegramModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TelegramListAdapter extends RecyclerView.Adapter<TelegramListAdapter.UserHolder> {
    Context context;
    ArrayList<TelegramModel> telegramList;
    HistoryAdapter.OnViewClickListener onViewClickListener;


    public TelegramListAdapter(Context context, ArrayList<TelegramModel> telegramList, HistoryAdapter.OnViewClickListener onViewClickListener) {
        this.context = context;
        this.telegramList = telegramList;
        this.onViewClickListener = onViewClickListener;
    }

    @NonNull
    @Override
    public UserHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UserHolder(LayoutInflater.from(context).inflate(R.layout.row_telegram_details, null));
    }

    @Override
    public void onBindViewHolder(@NonNull UserHolder holder, int position) {
        holder.tvUserId.setText(telegramList.get(position).getUserId());
        holder.tvDate.setText(telegramList.get(position).getDate());
        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onViewClickListener != null) {
                    onViewClickListener.onEditClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return telegramList.size();
    }

    class UserHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvDate)
        AppCompatTextView tvDate;
        @BindView(R.id.tvUserid)
        AppCompatTextView tvUserId;
        @BindView(R.id.ivEdit)
        ImageView ivEdit;

        public UserHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            Typeface typeface = Typeface.createFromAsset(itemView.getContext().getAssets(),"fonts/Manjari-Regular.ttf");
            Typeface typeface1 = Typeface.createFromAsset(itemView.getContext().getAssets(),"fonts/Nunito-ExtraBold.ttf");
            tvUserId.setTypeface(typeface1);
            tvDate.setTypeface(typeface);
        }
    }
}
