package com.aswdc.wadirect.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.aswdc.wadirect.fragment.application.TelegramFragment;
import com.aswdc.wadirect.fragment.application.WhatsappBusinessFragment;
import com.aswdc.wadirect.fragment.application.WhatsappFragment;

import java.text.SimpleDateFormat;

public class ApplicationAdapter extends FragmentStatePagerAdapter {



    private String tabTitles[] = new String[]{"Whatsapp", "WA Business","Telegram"};
    private Context context;

    public ApplicationAdapter(@NonNull FragmentManager fm,Context context) {
        super(fm);
        this.context = context;
    }


    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return WhatsappFragment.newInstance(0,"Whatsapp");

            case 1:
                return WhatsappBusinessFragment.newInstance(1, "WA Business");

            case 2:
                return TelegramFragment.newInstance(2, "Telegram");

            default: return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }


    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

}


