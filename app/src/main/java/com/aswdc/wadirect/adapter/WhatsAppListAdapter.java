package com.aswdc.wadirect.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc.wadirect.R;
import com.aswdc.wadirect.model.WhatsAppModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WhatsAppListAdapter extends RecyclerView.Adapter<WhatsAppListAdapter.UserHolder> {
    Context context;
    ArrayList<WhatsAppModel> whatsAppList;
    HistoryAdapter.OnViewClickListener onViewClickListener;


    public WhatsAppListAdapter(Context context, ArrayList<WhatsAppModel> whatsAppList, HistoryAdapter.OnViewClickListener onViewClickListener) {
        this.context = context;
        this.whatsAppList = whatsAppList;
        this.onViewClickListener = onViewClickListener;
    }


    @NonNull
    @Override
    public UserHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new UserHolder(LayoutInflater.from(context).inflate(R.layout.row_whatsapp_details, null));
    }

    @Override
    public void onBindViewHolder(@NonNull UserHolder holder, int position) {

        holder.tvCountryCodeNumber.setText("+" + whatsAppList.get(position).getCountryCode() + "  " + whatsAppList.get(position).getPhoneNumber() );
        holder.tvMessage.setText(whatsAppList.get(position).getMessage());
        holder.tvDate.setText(whatsAppList.get(position).getDate());
        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onViewClickListener != null) {
                    onViewClickListener.onEditClick(position);
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return whatsAppList.size();
    }


    class UserHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvCountryCodeNumber)
        AppCompatTextView tvCountryCodeNumber;
        @BindView(R.id.tvMessage)
        AppCompatTextView tvMessage;
        @BindView(R.id.tvDate)
        AppCompatTextView tvDate;
        @BindView(R.id.ivEdit)
        ImageView ivEdit;

        public UserHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            Typeface typeface = Typeface.createFromAsset(itemView.getContext().getAssets(), "fonts/Manjari-Regular.ttf");
            Typeface typeface1 = Typeface.createFromAsset(itemView.getContext().getAssets(), "fonts/Nunito-ExtraBold.ttf");
            tvCountryCodeNumber.setTypeface(typeface1);
            tvMessage.setTypeface(typeface);
            tvDate.setTypeface(typeface);
        }
    }
}
