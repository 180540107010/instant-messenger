package com.aswdc.wadirect.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.aswdc.wadirect.fragment.history.TelegramHistoryFragment;
import com.aswdc.wadirect.fragment.history.WhatsappBusinessHistoryFragment;
import com.aswdc.wadirect.fragment.history.WhatsappHistoryFragment;

public class HistoryAdapter extends FragmentStatePagerAdapter {

    private String tabTitles[] = new String[]{"Whatsapp", "WA Business","Telegram"};
    private Context context;

    public HistoryAdapter(@NonNull FragmentManager fm,Context context) {
        super(fm);
        this.context = context;
    }

    public interface OnViewClickListener{
        void onEditClick(int position);
    }


    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return WhatsappHistoryFragment.newInstance(0,"Whatsapp");

            case 1:
                return WhatsappBusinessHistoryFragment.newInstance(1, "WA Business");

            case 2:
                return TelegramHistoryFragment.newInstance(2, "Telegram");

            default: return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}
