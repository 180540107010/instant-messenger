package com.aswdc.wadirect.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.aswdc.wadirect.R;

public class BaseActivity extends AppCompatActivity {

    public void setupActionBar(String title,boolean isBackArrow){
        Toolbar toolbar = findViewById(R.id.custom_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayHomeAsUpEnabled(isBackArrow);
//        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_arrow);
    }
}
