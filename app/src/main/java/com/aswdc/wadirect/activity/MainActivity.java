package com.aswdc.wadirect.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;

import com.aswdc.wadirect.R;
import com.aswdc.wadirect.adapter.ApplicationAdapter;
import com.aswdc.wadirect.util.Constant;
import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity {


    @BindView(R.id.tab_layout)
    public TabLayout tlApplication;
    @BindView(R.id.view_pager)
    ViewPager viewPager;

    int viewPagerPosition = 0;

    int[] tabIcons = {
            R.drawable.whatsapp,
            R.drawable.whatsappbusiness,
            R.drawable.telegram
    };
    ApplicationAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupActionBar("Instant Messenger",false);
        ButterKnife.bind(this);
        setUpViewPagerAdapter();

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            if (extras.containsKey("viewPagerPosition")) {
                 viewPagerPosition = extras.getInt("viewPagerPosition", 0);

            }
        }

        viewPager.setCurrentItem(viewPagerPosition);
        setupTabIcons();


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == ViewPager.SCROLL_STATE_IDLE)
                {
                        // Hide the keyboard.
                        ((InputMethodManager)getSystemService(INPUT_METHOD_SERVICE))
                                .hideSoftInputFromWindow(viewPager.getWindowToken(), 0);
                }
            }
        });
    }


    private void setUpViewPagerAdapter() {
        adapter = new ApplicationAdapter(getSupportFragmentManager(), this);
        viewPager.setAdapter(adapter);
        tlApplication.setupWithViewPager(viewPager);
    }

    private void setupTabIcons() {

        TabLayout tabLayout = findViewById(R.id.tab_layout);
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu,menu);
        return super.onCreateOptionsMenu(menu);

    }



    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if(item.getItemId() == R.id.iHistory) {
            Intent intent = new Intent(MainActivity.this, HistoryActivity.class);
                startActivity(intent);
        }
        if(item.getItemId() == R.id.iDevelopers){
            Intent intent = new Intent(MainActivity.this, DeveloperActivity.class);
            startActivity(intent);
        }
        if(item.getItemId() == R.id.iCheckForUpdate){
            Intent updateintent = new Intent("android.intent.action.VIEW", Uri.parse("market://details?id="+getPackageName()));
            startActivity(updateintent);
        }
        if(item.getItemId() == R.id.iMoreApps){
            Intent moreappsintent = new Intent("android.intent.action.VIEW",
                    Uri.parse("market://search?q=pub:Darshan+Institute+of+Engineering+%26+Technology"));
            startActivity(moreappsintent);
        }
        if(item.getItemId() == R.id.iShare){
            Intent share = new Intent();
            share.setAction("android.intent.action.SEND");
            share.setType("text/plain");
            share.putExtra("android.intent.extra.TEXT", Constant.AppPlayStoreLink );
            startActivity(share);
        }
        return super.onOptionsItemSelected(item);
    }


}