package com.aswdc.wadirect.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;

import com.aswdc.wadirect.R;
import com.aswdc.wadirect.adapter.ApplicationAdapter;
import com.aswdc.wadirect.adapter.HistoryAdapter;
import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryActivity extends BaseActivity {
    @BindView(R.id.tab_layout)
    public TabLayout tlApplication;
    @BindView(R.id.view_pager)
    ViewPager viewPager;

    private boolean isChecked = false;

    int[] tabIcons = {
            R.drawable.whatsapp,
            R.drawable.whatsappbusiness,
            R.drawable.telegram
    };

    HistoryAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupActionBar("Recent History",true);
        ButterKnife.bind(this);
        setUpViewPagerAdapter();
        setupTabIcons();
    }

    private void setUpViewPagerAdapter() {
        adapter = new HistoryAdapter(getSupportFragmentManager(), this);
        viewPager.setAdapter(adapter);
        tlApplication.setupWithViewPager(viewPager);
    }

    private void setupTabIcons() {

        TabLayout tabLayout = findViewById(R.id.tab_layout);
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home){
           // onBackPressed();

            Intent launchNextActivity = new Intent(HistoryActivity.this, MainActivity.class);
            launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(launchNextActivity);
        }
        return super.onOptionsItemSelected(item);
    }

//
}
