package com.aswdc.wadirect.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.aswdc.wadirect.model.TelegramModel;

import java.util.ArrayList;

public class TblTelegram extends MyDatabase {

    public final static String TABLE_NAME = "TblTelegram";
    public final static String ID = "Id";
    public final static String USER_ID = "UserId";
    public final static String DATE = "Date";

    public TblTelegram(Context context) {
        super(context);
    }

    public ArrayList<TelegramModel> getTelegramList(){
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<TelegramModel> telegramList = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME + " ORDER BY " + DATE + " DESC ";
        Cursor cursor = db.rawQuery(query,null);
        if(cursor!=null){
            cursor.moveToFirst();
            for(int i=0;i<cursor.getCount();i++){
                telegramList.add(getCreatedTelegramModel(cursor));
                cursor.moveToNext();
            }
        }
        cursor.close();
        db.close();
        return telegramList;
    }

    public TelegramModel getCreatedTelegramModel(Cursor cursor){
        TelegramModel telegramModel = new TelegramModel();
        telegramModel.setId(cursor.getInt(cursor.getColumnIndex(ID)));
        telegramModel.setUserId(cursor.getString(cursor.getColumnIndex(USER_ID)));
        telegramModel.setDate(cursor.getString(cursor.getColumnIndex(DATE)));
        return telegramModel;
    }

    public long sendOnTelegram(String userId,String date){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(USER_ID,userId);
        cv.put(DATE,date);
        long insertedId = db.insert(TABLE_NAME,null,cv);
        db.close();
        return insertedId;
    }

    public int updateTelegramDetail(String userId,String date, int Id){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(USER_ID,userId);
        cv.put(DATE,date);
        int updatedTelegramId = db.update(TABLE_NAME,cv,ID + " = ?",new String[]{String.valueOf(Id)});
        db.close();
        return updatedTelegramId;
    }

}
