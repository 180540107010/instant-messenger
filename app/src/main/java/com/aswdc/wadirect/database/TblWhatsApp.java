package com.aswdc.wadirect.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.aswdc.wadirect.adapter.WhatsAppListAdapter;
import com.aswdc.wadirect.model.WhatsAppModel;

import java.util.ArrayList;

public class TblWhatsApp extends MyDatabase {

    public final static String TABLE_NAME = "TblWhatsApp";
    public final static String ID = "Id";
    public final static String COUNTRY_CODE = "CountryCode";
    public final static String COUNTRY_NAME = "CountryName";
    public final static String PHONE_NUMBER = "PhoneNumber";
    public final static String MESSAGE = "Message";
    public  final static String DATE = "Date";

    public TblWhatsApp(Context context) {
        super(context);
    }

    public ArrayList<WhatsAppModel> getWhatsAppList(){
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<WhatsAppModel> whatsAppList = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME + " ORDER BY " + DATE + " DESC " ;
        Cursor cursor = db.rawQuery(query,null);
        if(cursor!=null){
            cursor.moveToFirst();
            for(int i=0 ; i<cursor.getCount(); i++){
                whatsAppList.add(getCreatedWhatsAppModel(cursor));
                cursor.moveToNext();
            }
        }
        cursor.close();
        db.close();
        return whatsAppList;
    }

    public WhatsAppModel getCreatedWhatsAppModel(Cursor cursor) {
        WhatsAppModel whatsAppModel = new WhatsAppModel();
        whatsAppModel.setId(cursor.getInt(cursor.getColumnIndex(ID)));
        whatsAppModel.setCountryCode(cursor.getInt(cursor.getColumnIndex(COUNTRY_CODE)));
        whatsAppModel.setCountryName(cursor.getString(cursor.getColumnIndex(COUNTRY_NAME)));
        whatsAppModel.setPhoneNumber(cursor.getString(cursor.getColumnIndex(PHONE_NUMBER)));
        whatsAppModel.setMessage(cursor.getString(cursor.getColumnIndex(MESSAGE)));
        whatsAppModel.setDate(cursor.getString(cursor.getColumnIndex(DATE)));
        return whatsAppModel;
    }

    public long sendOnWhatsApp(int countryCode,String country,String phoneNumber, String message,String date){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(COUNTRY_CODE,countryCode);
        cv.put(COUNTRY_NAME,country);
        cv.put(PHONE_NUMBER,phoneNumber);
        cv.put(MESSAGE,message);
        cv.put(DATE,date);
        long insertedId = db.insert(TABLE_NAME,null,cv);
        db.close();
        return insertedId;
    }

    public int updateWhatsAppDetail(String phoneNumber, String message, String time,String date, int contactId){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(PHONE_NUMBER,phoneNumber);
        cv.put(MESSAGE,message);
        cv.put(DATE,date);
        int updatedWhatsAppId = db.update(TABLE_NAME,cv,ID + " = ?",new String[]{String.valueOf(contactId)});
        db.close();
        return updatedWhatsAppId;
    }

    public boolean checkIfNumberAlreadyOnList(String phoneNumber){
        Cursor cursor = null;
        SQLiteDatabase db = getReadableDatabase();
        String checkQuery = "SELECT " + PHONE_NUMBER + " FROM " + TABLE_NAME + " WHERE " + PHONE_NUMBER + " = ?";
        cursor= db.rawQuery(checkQuery,new String[]{String.valueOf(phoneNumber)});
        boolean exists = (cursor.getCount() > 0);
        int id;
        WhatsAppListAdapter whatsAppList;
        if(exists){
            for (int i =0;i<cursor.getColumnCount();i++){
            }
        }
        cursor.close();
        return exists;
    }
}
