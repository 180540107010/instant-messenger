package com.aswdc.wadirect.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.aswdc.wadirect.model.WhatsAppBusinessModel;

import java.util.ArrayList;

public class TblWhatsAppBusiness extends MyDatabase {
    public final static String TABLE_NAME = "TblWhatsAppBusiness";
    public final static String ID = "Id";
    public final static String COUNTRY_CODE = "CountryCode";
    public final static String COUNTRY_NAME = "CountryName";
    public final static String PHONE_NUMBER = "PhoneNumber";
    public final static String MESSAGE = "Message";

    public final static String DATE = "Date";

    public TblWhatsAppBusiness(Context context) {
        super(context);
    }

    public ArrayList<WhatsAppBusinessModel> getWhatsAppBusinessList(){
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<WhatsAppBusinessModel> whatsAppBusinessList = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME + " ORDER BY " + DATE + " DESC " ;
        Cursor cursor = db.rawQuery(query,null);
        if(cursor!=null){
            cursor.moveToFirst();
            for(int i=0 ; i<cursor.getCount(); i++){
                whatsAppBusinessList.add(getCreatedWhatsAppBusinessModel(cursor));
                cursor.moveToNext();
            }
        }
        cursor.close();
        db.close();
        return whatsAppBusinessList;
    }

    public WhatsAppBusinessModel getCreatedWhatsAppBusinessModel(Cursor cursor) {
        WhatsAppBusinessModel whatsAppBusinessModel = new WhatsAppBusinessModel();
        whatsAppBusinessModel.setId(cursor.getInt(cursor.getColumnIndex(ID)));
        whatsAppBusinessModel.setCountryCode(cursor.getInt(cursor.getColumnIndex(COUNTRY_CODE)));
        whatsAppBusinessModel.setCountryName(cursor.getString(cursor.getColumnIndex(COUNTRY_NAME)));
        whatsAppBusinessModel.setPhoneNumber(cursor.getString(cursor.getColumnIndex(PHONE_NUMBER)));
        whatsAppBusinessModel.setMessage(cursor.getString(cursor.getColumnIndex(MESSAGE)));
        whatsAppBusinessModel.setDate(cursor.getString(cursor.getColumnIndex(DATE)));
        return whatsAppBusinessModel;
    }

    public long sendOnWhatsAppBusiness(int countryCode,String country,String phoneNumber, String message,String date){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(COUNTRY_CODE,countryCode);
        cv.put(COUNTRY_NAME,country);
        cv.put(PHONE_NUMBER,phoneNumber);
        cv.put(MESSAGE,message);

        cv.put(DATE,date);
        long insertedId = db.insert(TABLE_NAME,null,cv);
        db.close();
        return insertedId;
    }

    public int updateWhatsAppBusinessDetail(String phoneNumber, String message,String date, int contactId){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(PHONE_NUMBER,phoneNumber);
        cv.put(MESSAGE,message);

        cv.put(DATE,date);
        int updatedWhatsAppBusinessId = db.update(TABLE_NAME,cv,ID + " = ?",new String[]{String.valueOf(contactId)});
        db.close();
        return updatedWhatsAppBusinessId;
    }
}
