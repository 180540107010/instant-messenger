package com.aswdc.wadirect.util;

public class Constant {


    public static final String APPLICATION = "Application";
    public static final String WHATSAPP_STRING = "Whatsapp";
    public static final String WHATSAPP_BUSINESS_STRING = "WA Business";
    public static final String TELEGRAM_STRING = "Telegram";

    public static final String WHATSAPP_OBJECT = "WhatsAppObject";
    public static final String WHATSAPP_BUSINESS_OBJECT = "WhatsAppBusinessObject";
    public static final String TELEGRAM_OBJECT = "TelegramObject";

    public static final String AdminEmailAddress="aswdc@darshan.ac.in";
    public static final String ASWDCEmailAddress="aswdc@darshan.ac.in";
    public static final String AdminMobileNo="+91-8200739115";
    public static final String AppPlayStoreLink="https://play.google.com/store/apps/details?id=com.aswdc_gtu_mcq&hl=en";
    public static final String SharedMessage="Download GTU MCQ App for B.E. 1st Year subject of GTU. Android: http://tiny.cc/agtumcq and  iPhone: http://tiny.cc/igtumcq";



}
