package com.aswdc.wadirect.fragment.application;

import android.accessibilityservice.AccessibilityService;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;

import com.aswdc.wadirect.activity.MainActivity;
import com.aswdc.wadirect.adapter.WhatsAppListAdapter;
import com.aswdc.wadirect.database.TblWhatsApp;
import com.aswdc.wadirect.model.WhatsAppModel;
import com.aswdc.wadirect.util.Constant;
import com.aswdc.wadirect.R;
import com.hbb20.CCPCountry;
import com.hbb20.CountryCodePicker;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;

public class WhatsappFragment extends ApplicationFragment  {

    Context context;
    WhatsAppModel whatsAppModel;

    @BindView(R.id.tvIntro)
    AppCompatTextView tvIntro;
    @BindView(R.id.ccp)
    CountryCodePicker ccp;
    @BindView(R.id.etPhoneNumber)
    AppCompatEditText etPhoneNumber;
    @BindView(R.id.ivClearPhoneNumber)
    ImageView ivClearPhoneNumber;
    @BindView(R.id.etMessage)
    AppCompatEditText etMessage;
    @BindView(R.id.ivClearMessage)
    ImageView ivClearMessage;
    @BindView(R.id.btnSendWhatsappMessage)
    Button btnSendWhatsappMessage;

    private String title;
    private int tab;
    boolean hasFocus = false;
    WhatsAppListAdapter whatsAppListAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_whatsapp, null);
        ButterKnife.bind(this, v);
        onClickListeners();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        getData();
        return v;

    }


    void getData(){
        if(getActivity().getIntent().hasExtra(Constant.WHATSAPP_OBJECT)){
            whatsAppModel = (WhatsAppModel) getActivity().getIntent().getSerializableExtra(Constant.WHATSAPP_OBJECT);
            ccp.setCountryForPhoneCode(whatsAppModel.getCountryCode());
            etPhoneNumber.setText(whatsAppModel.getPhoneNumber());
            etMessage.setText(whatsAppModel.getMessage());
        }
    }

    void onClickListeners(){

        etPhoneNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view.getId() == etPhoneNumber.getId()){
                    etPhoneNumber.setCursorVisible(true);
                }
            }
        });

        etMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view.getId() == etMessage.getId()){
                    etMessage.setCursorVisible(true);
                }
            }
        });


        etPhoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(etPhoneNumber.getText().length() < 1){
                    ivClearPhoneNumber.setVisibility(View.GONE);
                }
                else{
                    ivClearPhoneNumber.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(etMessage.getText().length() < 1){
                    ivClearMessage.setVisibility(View.GONE);
                }
                else{
                    ivClearMessage.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }




    public static WhatsappFragment newInstance(int tab, String title) {
        WhatsappFragment whatsappFragment = new WhatsappFragment();
        Bundle args = new Bundle();
        args.putInt("tabNo", tab);
        args.putString("appTitle", title);
        whatsappFragment.setArguments(args);
        return whatsappFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tab = getArguments().getInt("tabNo");
        title = getArguments().getString("appTitle");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvIntro.setTypeface(setTypefaceOnText());
        etPhoneNumber.setTypeface(setTypefaceOnText());
     etMessage.setTypeface(setTypefaceOnText());
        btnSendWhatsappMessage.setTypeface(setTypefaceOnButton());
    }

    @OnClick(R.id.btnSendWhatsappMessage)
    public void onViewClicked() {
        if (isValid()) {
            int countryCode = ccp.getSelectedCountryCodeAsInt();
            String phoneNo = etPhoneNumber.getText().toString();
            String countryName = ccp.getSelectedCountryName();
            ccp.registerCarrierNumberEditText(etPhoneNumber);
            String phoneNumber = ccp.getFullNumber();
            String message = etMessage.getText().toString();
            String whatsapp_package = "com.whatsapp";

            if (checkAppInstalledOrNot(whatsapp_package)) {
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(String.format("https://api.whatsapp.com/send?phone=%s&text=%s", phoneNumber, message)));
                intent.setPackage(whatsapp_package);
                startActivity(intent);

                long insertedId =new TblWhatsApp(getActivity().getApplicationContext()).sendOnWhatsApp(countryCode,countryName,phoneNo,
                        etMessage.getText().toString(),getCurrentDate());
                if(insertedId > 0){

                }
                else {
                    showToast("Something Went Wrong!");
                }


            } else {
                openPlayStore(whatsapp_package);
                appNotInstalledToast(Constant.WHATSAPP_STRING);
            }

        }
    }

    private boolean isValid() {
        boolean valid = true;

        if (etPhoneNumber.getText().length() < 10) {
            etPhoneNumber.setError("Invalid Phone Number");
            valid = false;
            if (TextUtils.isEmpty(etPhoneNumber.getText())) {
                etPhoneNumber.setError("No Phone Number");
                valid = false;
            }
        }
        return valid;
    }


    @OnClick(R.id.ivClearPhoneNumber)
    public void onIvClearPhoneNumberClicked() {
        etPhoneNumber.setText("");
    }

    @OnClick(R.id.ivClearMessage)
    public void onIvClearMessageClicked() {
        etMessage.setText("");
    }

}