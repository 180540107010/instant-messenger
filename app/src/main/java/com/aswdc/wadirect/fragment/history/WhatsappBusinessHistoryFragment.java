package com.aswdc.wadirect.fragment.history;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc.wadirect.R;
import com.aswdc.wadirect.activity.MainActivity;
import com.aswdc.wadirect.adapter.HistoryAdapter;
import com.aswdc.wadirect.adapter.WhatsAppBusinessListAdapter;
import com.aswdc.wadirect.database.TblWhatsAppBusiness;
import com.aswdc.wadirect.fragment.application.WhatsappBusinessFragment;
import com.aswdc.wadirect.model.WhatsAppBusinessModel;
import com.aswdc.wadirect.util.Constant;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WhatsappBusinessHistoryFragment extends HistoryFragment {
    WhatsAppBusinessListAdapter whatsAppBusinessListAdapter;
    ArrayList<WhatsAppBusinessModel> whatsAppBusinessList = new ArrayList<>();

    @BindView(R.id.rvWhatsapp)
    RecyclerView rvWhatsappBusiness;
    @BindView(R.id.tvNoHistory)
    TextView tvNoHistory;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_whatsapp_history, null);
        ButterKnife.bind(this, v);
        setAdapter();
        return v;
    }

    public static WhatsappBusinessHistoryFragment newInstance(int tab, String title) {
        WhatsappBusinessHistoryFragment whatsappBusinessHistoryFragment = new WhatsappBusinessHistoryFragment();
        Bundle args = new Bundle();
        args.putInt("tabNo", tab);
        args.putString("appTitle", title);
        whatsappBusinessHistoryFragment.setArguments(args);
        return whatsappBusinessHistoryFragment;
    }



    void setAdapter() {
//        whatsAppBusinessList.clear();
        rvWhatsappBusiness.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        whatsAppBusinessList.addAll(new TblWhatsAppBusiness(getActivity()).getWhatsAppBusinessList());
        whatsAppBusinessListAdapter = new WhatsAppBusinessListAdapter(getActivity(), whatsAppBusinessList, new HistoryAdapter.OnViewClickListener() {
            @Override
            public void onEditClick(int position) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.putExtra("CountryCode" , whatsAppBusinessList.get(position).getCountryCode());
                intent.putExtra("PhoneNumber" , whatsAppBusinessList.get(position).getPhoneNumber());
                intent.putExtra("Message" , whatsAppBusinessList.get(position).getMessage());
                intent.putExtra(Constant.WHATSAPP_BUSINESS_OBJECT,whatsAppBusinessList.get(position));
                intent.putExtra("viewPagerPosition",1);
                startActivity(intent);
            }
        });
        rvWhatsappBusiness.setAdapter(whatsAppBusinessListAdapter);
        checkAndVisibleHistory();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvNoHistory.setTypeface(setTypefaceOnText());
    }

    public void checkAndVisibleHistory() {
        if (whatsAppBusinessList.size() > 0) {
            tvNoHistory.setVisibility(View.GONE);
            rvWhatsappBusiness.setVisibility(View.VISIBLE);
        } else {
            tvNoHistory.setVisibility(View.VISIBLE);
            rvWhatsappBusiness.setVisibility(View.GONE);
        }
    }

}
