package com.aswdc.wadirect.fragment.application;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;

import com.aswdc.wadirect.activity.MainActivity;
import com.aswdc.wadirect.adapter.WhatsAppBusinessListAdapter;
import com.aswdc.wadirect.database.TblWhatsApp;
import com.aswdc.wadirect.database.TblWhatsAppBusiness;
import com.aswdc.wadirect.model.WhatsAppBusinessModel;
import com.aswdc.wadirect.model.WhatsAppModel;
import com.aswdc.wadirect.util.Constant;
import com.aswdc.wadirect.R;
import com.hbb20.CountryCodePicker;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WhatsappBusinessFragment extends ApplicationFragment {

    @BindView(R.id.ccp)
    CountryCodePicker ccp;
    @BindView(R.id.etPhoneNumber)
    AppCompatEditText etPhoneNumber;
    @BindView(R.id.ivClearPhoneNumber)
    ImageView ivClearPhoneNumber;
    @BindView(R.id.etMessage)
    AppCompatEditText etMessage;
    @BindView(R.id.ivClearMessage)
    ImageView ivClearMessage;
    @BindView(R.id.btnSendWhatsappBusinessMessage)
    Button btnSendWhatsappBusiness;
    @BindView(R.id.tvIntro)
    AppCompatTextView tvIntro;
    @BindView(R.id.fragment_wabusiness)
    LinearLayout frWABusiness;

    private String title;
    private int tab;

    WhatsAppBusinessModel whatsAppBusinessModel;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_whatsappbusiness, null);
        ButterKnife.bind(this, v);
        onClickListeners();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        getData();
        return v;
    }


    public static WhatsappBusinessFragment newInstance(int tab, String title) {
        WhatsappBusinessFragment whatsappBusinessFragment = new WhatsappBusinessFragment();
        Bundle args = new Bundle();
        args.putInt("tabNo", tab);
        args.putString("appTitle", title);
        whatsappBusinessFragment.setArguments(args);
        return whatsappBusinessFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tab = getArguments().getInt("tabNo");
        title = getArguments().getString("appTitle");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvIntro.setTypeface(setTypefaceOnText());
        etPhoneNumber.setTypeface(setTypefaceOnText());
        etMessage.setTypeface(setTypefaceOnText());
        btnSendWhatsappBusiness.setTypeface(setTypefaceOnButton());

    }

    void getData(){
        if(getActivity().getIntent().hasExtra(Constant.WHATSAPP_BUSINESS_OBJECT)){
            whatsAppBusinessModel = (WhatsAppBusinessModel) getActivity().getIntent().getSerializableExtra(Constant.WHATSAPP_BUSINESS_OBJECT);
            ccp.setCountryForPhoneCode(whatsAppBusinessModel.getCountryCode());
            etPhoneNumber.setText(whatsAppBusinessModel.getPhoneNumber());
            etMessage.setText(whatsAppBusinessModel.getMessage());
        }
    }

    void onClickListeners(){
        etPhoneNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view.getId() == etPhoneNumber.getId()){
                    etPhoneNumber.setCursorVisible(true);
                }
            }
        });

        etMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view.getId() == etMessage.getId()){
                    etMessage.setCursorVisible(true);
                }
            }
        });
        etPhoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(etPhoneNumber.getText().length() < 1){
                    ivClearPhoneNumber.setVisibility(View.GONE);
                }
                else{
                    ivClearPhoneNumber.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        etMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(etMessage.getText().length() < 1){
                    ivClearMessage.setVisibility(View.GONE);
                }
                else{
                    ivClearMessage.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    @OnClick(R.id.btnSendWhatsappBusinessMessage)
    public void onViewClicked() {
        if (isValid()) {
            int countryCode = ccp.getSelectedCountryCodeAsInt();
            String countryName = ccp.getSelectedCountryName();
            ccp.registerCarrierNumberEditText(etPhoneNumber);
            String phoneNumber = ccp.getFullNumber();
            String message = etMessage.getText().toString();

            String whatsappBusiness_package = "com.whatsapp.w4b";

            if (checkAppInstalledOrNot(whatsappBusiness_package)) {
               Intent intent = new Intent(Intent.ACTION_VIEW,
                       Uri.parse(String.format("https://wa.me/%s?text=%s", phoneNumber, message)));
                intent.setPackage(whatsappBusiness_package);
                startActivity(intent);

                long insertedId = new TblWhatsAppBusiness(getActivity().getApplicationContext()).sendOnWhatsAppBusiness(countryCode,countryName,phoneNumber,
                            etMessage.getText().toString(), getCurrentDate());
                if(insertedId > 0){

                }
                else {
                    showToast("Something Went Wrong!");
                }

            } else {
                openPlayStore(whatsappBusiness_package);
                appNotInstalledToast(Constant.WHATSAPP_BUSINESS_STRING);
            }

        }
    }

    private boolean isValid() {
        boolean valid = true;

        if (etPhoneNumber.getText().length() < 10) {
            etPhoneNumber.setError("Invalid Phone Number");
            valid = false;
            if (TextUtils.isEmpty(etPhoneNumber.getText())) {
                etPhoneNumber.setError("No Phone Number");
                valid = false;
            }
        }
        return valid;
    }

    @OnClick(R.id.ivClearMessage)
    public void onIvClearMessageClicked() {
        etMessage.setText("");
    }


    @OnClick(R.id.ivClearPhoneNumber)
    public void onIvClearPhoneNumberClicked() {
        etPhoneNumber.setText("");
    }


}
