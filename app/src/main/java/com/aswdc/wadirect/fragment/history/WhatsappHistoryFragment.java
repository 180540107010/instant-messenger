package com.aswdc.wadirect.fragment.history;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc.wadirect.R;
import com.aswdc.wadirect.activity.MainActivity;
import com.aswdc.wadirect.adapter.HistoryAdapter;
import com.aswdc.wadirect.adapter.WhatsAppListAdapter;
import com.aswdc.wadirect.database.TblWhatsApp;
import com.aswdc.wadirect.model.WhatsAppModel;
import com.aswdc.wadirect.util.Constant;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class WhatsappHistoryFragment extends HistoryFragment {

    ArrayList<WhatsAppModel> whatsAppList = new ArrayList<>();
    WhatsAppListAdapter whatsAppListAdapter;

    @BindView(R.id.rvWhatsapp)
    RecyclerView rvWhatsapp;
    @BindView(R.id.tvNoHistory)
    TextView tvNoHistory;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_whatsapp_history, null);
        ButterKnife.bind(this, v);
        setAdapter();
        return v;

    }


    public static WhatsappHistoryFragment newInstance(int tab, String title) {
        WhatsappHistoryFragment whatsappHistoryFragment = new WhatsappHistoryFragment();
        Bundle args = new Bundle();
        args.putInt("tabNo", tab);
        args.putString("appTitle", title);
        whatsappHistoryFragment.setArguments(args);
        return whatsappHistoryFragment;
    }

    void setAdapter() {
        rvWhatsapp.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        whatsAppList.addAll(new TblWhatsApp(getActivity()).getWhatsAppList());
        whatsAppListAdapter = new WhatsAppListAdapter(getActivity(), whatsAppList, new HistoryAdapter.OnViewClickListener() {
            @Override
            public void onEditClick(int position) {

                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.putExtra("CountryCode" , whatsAppList.get(position).getCountryCode());
                intent.putExtra("PhoneNumber" , whatsAppList.get(position).getPhoneNumber());
                intent.putExtra("Message" , whatsAppList.get(position).getMessage());
                intent.putExtra(Constant.WHATSAPP_OBJECT,whatsAppList.get(position));
                intent.putExtra("viewPagerPosition",0);
                startActivity(intent);
            }
        });
        rvWhatsapp.setAdapter(whatsAppListAdapter);
        checkAndVisibleHistory();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvNoHistory.setTypeface(setTypefaceOnText());
    }

    public void checkAndVisibleHistory() {
        if (whatsAppList.size() > 0) {
            tvNoHistory.setVisibility(View.GONE);
            rvWhatsapp.setVisibility(View.VISIBLE);
        } else {
            tvNoHistory.setVisibility(View.VISIBLE);
            rvWhatsapp.setVisibility(View.GONE);
        }
    }
}
