package com.aswdc.wadirect.fragment.application;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.aswdc.wadirect.R;
import com.aswdc.wadirect.activity.MainActivity;
import com.aswdc.wadirect.adapter.TelegramListAdapter;
import com.aswdc.wadirect.database.TblTelegram;
import com.aswdc.wadirect.database.TblWhatsApp;
import com.aswdc.wadirect.model.TelegramModel;
import com.aswdc.wadirect.model.WhatsAppModel;
import com.aswdc.wadirect.util.Constant;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TelegramFragment extends ApplicationFragment{

    @BindView(R.id.etUserId)
    AppCompatEditText etUserId;
    @BindView(R.id.ivClearUserId)
    ImageView ivClearUserId;
    @BindView(R.id.btnSendTelegramMessage)
    Button btnSendTelegramMessage;
    @BindView(R.id.tvIntro)
    AppCompatTextView tvIntro;
    @BindView(R.id.fragment_telegram)
    LinearLayout telegram;
    private String title;
    private int tab;
    TelegramModel telegramModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_telegram, null);
        ButterKnife.bind(this, v);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        onClickListeners();
        getData();
        return v;
    }

    void getData(){
        if(getActivity().getIntent().hasExtra(Constant.TELEGRAM_OBJECT)){
            telegramModel = (TelegramModel) getActivity().getIntent().getSerializableExtra(Constant.TELEGRAM_OBJECT);
            etUserId.setText(telegramModel.getUserId());
        }
    }


    public static TelegramFragment newInstance(int tab, String title) {
        TelegramFragment telegramFragment = new TelegramFragment();
        Bundle args = new Bundle();
        args.putInt("tabNo", tab);
        args.putString("appTitle", title);
        telegramFragment.setArguments(args);
        return telegramFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tab = getArguments().getInt("tabNo");
        title = getArguments().getString("appTitle");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvIntro.setTypeface(setTypefaceOnText());
        etUserId.setTypeface(setTypefaceOnText());
        btnSendTelegramMessage.setTypeface(setTypefaceOnButton());
    }

    void onClickListeners(){

        etUserId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view.getId() == etUserId.getId()){
                    etUserId.setCursorVisible(true);
                }
            }
        });

        etUserId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                if(etUserId.getText().length() < 1){
                    ivClearUserId.setVisibility(View.GONE);
                }
                else{
                    ivClearUserId.setVisibility(View.VISIBLE);
                }
                if(etUserId.getText().length()>0) {
                    boolean valid = isValid();
                    if (valid) {
                        etUserId.setError(null);
                    }
                }


            }

            @Override
            public void afterTextChanged(Editable editable) {


            }
        });

    }


    @OnClick(R.id.btnSendTelegramMessage)
    public void onViewClicked() {
        if (isValid()) {
            String telegram_package = "org.telegram.messenger";
            String userId = etUserId.getText().toString();

            if (checkAppInstalledOrNot(telegram_package)) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://t.me/" + userId));
                intent.setPackage(telegram_package);
                startActivity(intent);
                long insertedId = new TblTelegram(getActivity().getApplicationContext()).sendOnTelegram(userId,getCurrentDate());
                if(insertedId > 0){

                }
                else {
                    showToast("Something Went Wrong!");
                }

            } else {
                openPlayStore(telegram_package);
                appNotInstalledToast(Constant.TELEGRAM_STRING);
            }
        }
    }

    private boolean isValid() {
        boolean valid = true;
        if (TextUtils.isEmpty(etUserId.getText().toString())) {
            valid = false;
            etUserId.setError("Enter user id");
        }
        if(etUserId.getText().toString().contains(" ")){
            valid = false;
            etUserId.setError("Space not allowed");
        }
        return valid;
    }

    @OnClick(R.id.ivClearUserId)
    public void onIvClearUserIdClicked() {
        etUserId.setText("");
    }


}
