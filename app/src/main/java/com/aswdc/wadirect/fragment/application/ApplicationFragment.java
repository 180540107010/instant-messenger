package com.aswdc.wadirect.fragment.application;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class ApplicationFragment extends Fragment {

    public void appNotInstalledToast(String applicationName) {
        Toast.makeText(getContext(),"Please install " + applicationName +  " in your phone.",Toast.LENGTH_SHORT).show();
    }

    public boolean checkAppInstalledOrNot(String uri) {
        PackageManager pm = getActivity().getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) { }

        return false;
    }


    public void openPlayStore(String applicationPackage){
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + applicationPackage)));

    }

    public Typeface setTypefaceOnText() {
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/Manjari-Regular.ttf");
        return typeface;
    }

    public Typeface setTypefaceOnButton() {
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/Nunito-ExtraBold.ttf");
        return typeface;
    }

    public void showToast(String message){
        Toast.makeText(getContext(),message,Toast.LENGTH_SHORT).show();
    }

    public String getCurrentDate(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm a ");
        Date current = Calendar.getInstance().getTime();
        String result = sdf.format(current);
        return result;
    }

    public String getCurrentTime(){
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
        Date current = Calendar.getInstance().getTime();
        String result = sdf.format(current);
        return result;
    }


}
