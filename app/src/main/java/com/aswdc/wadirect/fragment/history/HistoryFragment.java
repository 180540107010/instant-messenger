package com.aswdc.wadirect.fragment.history;

import android.graphics.Typeface;

import androidx.fragment.app.Fragment;

public class HistoryFragment extends Fragment {

    public Typeface setTypefaceOnText() {
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/Manjari-Regular.ttf");
        return typeface;
    }
}
