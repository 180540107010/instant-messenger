package com.aswdc.wadirect.fragment.history;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aswdc.wadirect.R;
import com.aswdc.wadirect.activity.MainActivity;
import com.aswdc.wadirect.adapter.HistoryAdapter;
import com.aswdc.wadirect.adapter.TelegramListAdapter;
import com.aswdc.wadirect.database.TblTelegram;
import com.aswdc.wadirect.fragment.application.TelegramFragment;
import com.aswdc.wadirect.model.TelegramModel;
import com.aswdc.wadirect.util.Constant;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TelegramHistoryFragment extends HistoryFragment {


    ArrayList<TelegramModel> telegramList = new ArrayList<>();
    TelegramListAdapter telegramListAdapter;
    @BindView(R.id.rvTelegram)
    RecyclerView rvTelegram;
    @BindView(R.id.tvNoHistory)
    TextView tvNoHistory;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_telegram_history, null);
        ButterKnife.bind(this, v);
        setAdapter();
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvNoHistory.setTypeface(setTypefaceOnText());
    }

    public static TelegramHistoryFragment newInstance(int tab, String title) {
        TelegramHistoryFragment telegramHistoryFragment = new TelegramHistoryFragment();
        Bundle args = new Bundle();
        args.putInt("tabNo", tab);
        args.putString("appTitle", title);
        telegramHistoryFragment.setArguments(args);
        return telegramHistoryFragment;
    }

    void setAdapter() {
        rvTelegram.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        telegramList.addAll(new TblTelegram(getActivity()).getTelegramList());
        telegramListAdapter = new TelegramListAdapter(getActivity(), telegramList, new HistoryAdapter.OnViewClickListener() {
            @Override
            public void onEditClick(int position) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.putExtra(Constant.TELEGRAM_OBJECT,telegramList.get(position));
                intent.putExtra("viewPagerPosition",2);
                startActivity(intent);
            }
        });
        rvTelegram.setAdapter(telegramListAdapter);
        checkAndVisibleHistory();
    }

    public void checkAndVisibleHistory() {
        if (telegramList.size() > 0) {
            tvNoHistory.setVisibility(View.GONE);
            rvTelegram.setVisibility(View.VISIBLE);
        } else {
            tvNoHistory.setVisibility(View.VISIBLE);
            rvTelegram.setVisibility(View.GONE);
        }
    }

}
